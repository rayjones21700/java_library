import java.util.HashMap;

public class LibraryTest {
    public static void main(String[] args) { 
        HashMap<String, Item> map = new HashMap<String, Item>(); 

        Library lib = new Library(map);
        Item book1 = new Book("1");
        lib.showInventory();
        lib.addItem(book1);
        lib.showInventory();
        lib.removeItem("1");
        lib.showInventory();
        
        for(int i = 0; i < 6; i++){
            Item newBook = new Book(""+i+"");
            lib.addItem(newBook);
        }
        lib.showInventory();
        for(int i = 7; i < 14; i++){
            Item newPeriodical = new Periodical(""+i+"");
            lib.addItem(newPeriodical);
        }
        lib.showInventory();
        lib.loanItem("1","4","01/02/03");
    } 
}