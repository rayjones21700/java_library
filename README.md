This project was created to replicate the process of a library. A central Library class is used to manage the borrowing of books, along with the adding and removing of various items. Magazines cannot be loaned out. 
A hash-map was used to save space and time while searching books for users to use, and ID numbers are assumed to be present for all items in library to for O (1) look up times. 
The current state is very bare bones and testing of methods is done using the main method. However, the functions are able to add variety of books and periodicals of different id numbers.

**Future Ideas**
Having a list of users and even abstract classes to differentiate between librarians and customers

ToString methods to display information of a given book
