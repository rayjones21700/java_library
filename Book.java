public class Book extends Item {
    String isbn;
    String author;
    String publisher;
    String numberPages;
    String location;

    
    public Book(String idNumber) {
        this.setIdNumber(idNumber);
    }
	@Override
	public boolean canCheckout() {
		return true;
	}

}