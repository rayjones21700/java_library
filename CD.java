public class CD extends Item {
    String producer;
    String author;
    String publisher;
    String numberPages;
    String location;

    public CD(String idNumber) {
        this.setIdNumber(idNumber);
    }
	@Override
	public boolean canCheckout() {
		return true;
    }

}