import java.util.HashMap;
import java.util.Map;

public class Library {
    //Collection of Items

    //Use Hashmap to look up and remove items
    Map<String,Item> items = new HashMap<String, Item>();
    public Library(Map<String, Item> items) {
        this.items = items;
    }

    //Methods
    public void loanItem(String futureOwner, String id, String dueDate){
        Item obj = this.items.get(id);
        if(obj.canCheckout()){
            obj.setLoaned(true);
            obj.setCurrentOwner(futureOwner);
            obj.setDueDate(dueDate);
        }
        else{
            System.out.print("Error, you may not checkout this item");
        }
    }
    public void removeItem(String id){
        if(this.items.get(id) != null){
            this.items.remove(id);
        }
    }
    public void addItem(Item obj){
        String id = obj.getIdNumber();
        if(this.items.get(id) == null){
            this.items.put(id,obj);
        }
        else{
            System.out.println("problem");
        }
    }
    public void showInventory(){
        System.out.println(this.items);
    }



  }