public class Periodical extends Item{
    String issueNumber;
    String publisher;
    String numberPages;

    public Periodical(String idNumber) {
        this.setIdNumber(idNumber);
    }

	@Override
	public boolean canCheckout() {
		return false;
	}
}