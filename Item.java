public abstract class Item {
    private String idNumber;
    private String name;
    private String location;

    private String dueDate;
    private boolean isLoaned;
    private String currentOwner;
    //Books will have authors, page number, etc. ISBN, location
    //CDs album, studio, creator, publisher
    //Periodicals can't be checkedout

    public abstract boolean canCheckout();
//Getters and Setters
	public String getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
    
    public void setDueDate(String dueDate){
        this.dueDate = dueDate;
    }
    public void setLoaned(boolean isLoaned){
        this.isLoaned = isLoaned;
    }
    public void setCurrentOwner(String currentOwner){
        this.currentOwner = currentOwner;
    }
	public String getDueDate() {
		return dueDate;
	}
	public boolean isLoaned() {
		return isLoaned;
	}
	public String getCurrentOwner() {
		return currentOwner;
    }
    public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}




  }